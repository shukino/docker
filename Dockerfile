FROM php:8.0-apache
# Aktifkan Module apache2
RUN a2enmod rewrite
RUN a2enmod deflate
# Install semua dependesi yang diperlukan
RUN apt-get update && apt-get install -y \
		libc-client-dev \
		libkrb5-dev \
		libcurl4-openssl-dev \
		libzip-dev libxslt-dev \
		nano \
		git \
		unzip \
		autoconf \
		pkg-config \
		zlib1g-dev \
		libicu-dev \
		g++ \
		libssl-dev \
		zlib1g-dev \
		libpng-dev \
		libjpeg-dev \
		libfreetype6-dev \
		&& apt-get clean -y && apt-get autoclean -y && apt-get autoremove -y \
        && rm -rf /var/cache/apt/archives/* /var/cache/apt/*.bin /var/lib/apt/lists/* \
        && rm -rf /usr/share/man/* && rm -rf /usr/share/doc/* \
        && touch /var/log/auth.log
# Aktifkan dan install php module
RUN docker-php-ext-install gd
RUN pecl install mongodb-1.15.0
RUN pecl install xdebug
RUN pecl install redis
RUN echo "extension=mongodb.so" >> /usr/local/etc/php/conf.d/mongodb.ini
RUN echo "extension=redis.so" >> /usr/local/etc/php/conf.d/redis.ini
RUN echo "zend_extension=xdebug.so\nxdebug.mode=debug\nxdebug.start_with_request=yes" >> /usr/local/etc/php/conf.d/xdebug.ini
RUN docker-php-ext-configure imap --with-kerberos --with-imap-ssl
RUN docker-php-ext-install imap
RUN docker-php-ext-install pdo pdo_mysql
RUN docker-php-ext-install fileinfo
RUN docker-php-ext-install curl
RUN docker-php-ext-install mysqli
RUN docker-php-ext-install xsl
RUN docker-php-ext-install zip
RUN docker-php-ext-install bcmath
RUN docker-php-ext-install opcache
# Install Composer
WORKDIR /var/www
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
# Pindahkan data yang diperlukan dari local ke container
RUN rm /etc/apache2/sites-available/000-default.conf
COPY apache2.conf/000-default.conf /etc/apache2/sites-available/000-default.conf